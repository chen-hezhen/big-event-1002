//切换注册和登录的盒子
$('.login').on('click','a',function(){
    $('.login').hide().next().show();
});
$('.register').on('click','a',function(){
    $('.register').hide().prev().show();
});

//此处特别要注意今后用遇到要用表单提交的情况，尽量使用表单注册提交事件
//注意如过要使用layui的内置模块form的verify方法，就必须通过给表单注册提交事件的方法来实现注册信息的提交，用点击事件无论是否符合正则表单验证，都会成功注册。
// // ------------- 实现注册 --------------
// //点击注册按钮，提交数据
// $('button:contains("注册")').click(function(e){
//     //阻止默认提交跳转事件
//     e.preventDefault();
//     //收集form表单提交的信息
//     let data = $('.register form').serialize();
//     //注册的接口文档要求 参数为用户名username和密码password
//     $.ajax({
//         type:'post',
//         url:'/api/reguser',
//         data,
//         success:function(res){
//             layer.msg(res.message);
//             if(res.status === 0){
//                 $('.register form')[0].reset();
//                 $('login').show().next().hide();
//             }
//         }
//     });
// });

//------------ 表单完成注册 -----------
//找到注册的表单，注册submit事件
$('.register form').on('submit',function(e){
    //阻止默认提交跳转事件
    e.preventDefault();
    //收集表单提交信息
   let data = $(this).serialize();
   //注册的接口文档，只需要收集用户名和密码，所以要把再次输入密码的name删除
   $.ajax({
       type:'post',
       url:'/api/reguser',
       data:data,
       success:function(res){
           layer.msg(res.message);
           if(res.status === 0){
               //清空输入框的值
               $('.register form')[0].reset();
               //注册成功让登录的盒子显示
               $('.login').show().next().hide();
           }
       }
   });
});

//------------ 注册表单的正则判断 -------
//使用layui的内置模块进行正则判断
//加载模块
let form = layui.form;
//调用form模块的verify方法进行表单验证，需要在表单元素上添加lay-verify=""，有多个值时，用｜隔开
form.verify({
    // 对象:键:值
    // 对象就是需要写在lay-verify =""里面的值
    //键就是验证规则名，值就是验证的方式方法（数组/函数）
    //值用数组的时候----键:['验证规则','验证不过的提示']
    //值用函数的话 ---- 一定要有形参和返回值
    /*函数的方法 形参value 指的是表单的值也就是用户输入的值
    return 返回值，是验证不通过的提示
      对象:function(表单的值,表单的dom对象){
              if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
        return '用户名不能有特殊字符';
      }  
      }
    */
   //用户名的正则判断
   username:function(value){
       if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
        return '用户名不能有特殊字符';
       }
       if(/(^\_)|(\__)|(\_+$)/.test(value)){
        return '用户名首尾不能出现下划线\'_\'';
      }
      if(/^\d+\d+\d$/.test(value)){
        return '用户名不能全为数字';
      }
   },
   //密码的正则判断
   // \S 除了空白符（换行符等）以外其他的字符
   pass:[/^[\S]{6,12}$/,'密码必须是6到12位的，不包含空格符的字符'],
   same:function(val){
       //此处是为了判断确认密码的输入值val === 用户首次输入的密码
       //获取用户输入的密码
       //jq的方法会自定将空白去除
       let pwd = $('.register .pwd').val();
       if(pwd !== val){
           return '两次输入的密码不同，请重新输入';
       }        
   },
});


//----------------------- 登录 ---------------------------
//找到登录的表单注册提交的事件
$('.login .layui-form').on('submit',function(e){
    e.preventDefault();
    let data = $(this).serialize();
    $.ajax({
        type:'post',
        url:'/api/login',
        data,
        success:function(res){
            layer.msg(res.message);
            if(res.status === 0){
                //登录成功，本地保存token
                localStorage.setItem('token',res.token);
                //跳转到后台首页index.html
                location.href = 'index.html';
            }
        }
    });
});