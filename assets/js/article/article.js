//----------- 引入分页和表单内置模块 ----------
let laypage = layui.laypage;
let form =layui.form;
let data = {
    pagenum:1,//获取第一页的数据
    pagesize:2,//每页显示两条数据
    cate_id:'',
    state:''
}
//-------------- ajax请求数据，渲染文章列表 --------------
function renderArticle(){
    $.ajax({
        url:'/my/article/list',
        data,
        success:function(res){
            let html = template('tpl-list',res);
            $('tbody').html(html);
            //ajax请求成功，将total传进去
            showPage(res.total);
            console.log(res);
        }
    });
}
renderArticle();

//--------------- 分页 渲染 -----------
function showPage(t){
    laypage.render({
        elem:'page',
        count:t,
        limit:data.pagesize,//每页显示的条数
        limits:[2,3,5,10], //下拉框更改每页显示数据条数
        curr:data.pagenum,// 起始页
        layout:['prev', 'page', 'next','count','limit','skip'],//上一个 页码 下一页 数据总数 下拉框更改页面显示数据数目 快速跳转页码 可以更改这些属性的顺序
        // 刷新页面,页面切换会触发jump函数，调用showPage就会自动调用jump函数，第一次调用first值为true
        jump:function(obj,first){
            //obj 包含了当前分页的所有的参数
            //first:是jump第一次触发显示true，之后触发first打印是undefined
            //为了避免死循环，因为第一次页面是在外面调用过了，所以不能在里面再调用了，所以要判断不是第一次调用了，不是第一次调用才能更换页面
            if(first === undefined){
            data.pagenum = obj.curr;
            data.pagesize = obj.limit;
            renderArticle();
            }
        }
      });
}
// showPage();

// --------------  发送请求，获取所有分类 --------------

$.ajax({
    url:'/my/article/cates',
    success:function(res){
        let html = template('tpl-category',res);
        $('select[name=category]').html(html);
        // 因为下拉框动态渲染的，所以需要调用layui的更新渲染方法
        form.render('select');
    }
});

//--------------- 完成筛选功能 -----------------
$('body').on('submit','.search',function(e){
    e.preventDefault();
    // 获取到的数据是一个数组
    let p = $(this).serializeArray();
    console.log(123);
    data.cate_id = p[0].value;
    data.state = p[1].value;
    //重置pagenum，筛选之后也是先看到第一页的数据
    data.pagenum= 1;
    renderArticle();
});

// 注册模版过滤器，注册之后，模版中就可以调用这个函数
//<td>{{val.pub_date|dateFormat}}</td>,注册过滤器会将｜前面的数据当作参数传入函数中
//注册之后函数也可以匿名，写为template.defaults.imports.dateFormat = function (str){
template.defaults.imports.dateFormat = function dateFormat(str){
    let date = new Date(str);
    let y = date.getFullYear();
    let m = addZero(date.getMonth()+1); 
    let d = addZero(date.getDate()) ;
    let h =addZero(date.getHours()) ;
    let i = addZero(date.getMinutes());
    let s =addZero(date.getSeconds()) ;
    return y + '-'+m+ '-'+d+' '+h +':'+i + ':'+s;
}
function addZero(n){
    return n < 10 ? '0'+ n : n;
}

//---------------- 删除文章 -----------------

$('body').on('click','button:contains("删除")',function(){
    // 获取id
    let id = $(this).data('id');
    //敏感操作询问
    layer.confirm('是否要删除？',function(index){
        //点击确定执行代码
        $.ajax({
            url:'/my/article/delete/'+id,
            success:function(res){
                layer.msg(res.status);
                if(res.status === 0){
                    renderArticle();
                }
            }
        });
        layer.close(index);
    });
});