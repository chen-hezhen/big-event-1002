//---------- 获取文章分类列表并渲染页面 ------------
function renderSort(){
    $.ajax({
        url:'/my/article/cates',
        success:function(res){
            //模版
            let html = template('tpl-list',res);
            $('tbody').html(html);
            console.log('文章列表渲染');
        }
    });
}
renderSort();

//------------ 删除分类 -------------
$('body').on('click','button:contains("删除")',function(){
    //根据自定义属性获取分类的id
    let id = $(this).data('id');
    //敏感操作提示
    layer.confirm('你确定要删除吗？',{icon:3,title:'提示'},function(index){
        $.ajax({
            url:'/my/article/deletecate/' + id,
            success:function(res){
                layer.msg(res.message);
                if(res.status === 0){
                    renderSort();
                }
            }
        });
        //删除成功清除弹层
        layer.close(index);
    });
});

//---------- 添加弹层 -----------
let indexadd;
$('button:contains("添加类别")').on('click',function(){
    indexadd = layer.open({
        //弹出层样式
        type:1,
        //弹出层标题
        title:'文章类别添加',
        //弹出层宽高
        area:['500px','250px'],
        content:$('#tpl-add').html(),
    });
});

//----------- 完成添加数据 ---------
$('body').on('submit','.form-add',function(e){
    //阻止表单默认跳转事件
    e.preventDefault();
    let data = $(this).serialize();
    $.ajax({
        type:'post',
        url:'/my/article/addcates',
        data,
        success:function(res){
            layer.msg(res.message);
            if(res.status === 0){
                renderSort();
                //添加成功，关闭添加弹层
                layer.close(indexadd);
            }
        }
    });
});

//------------ 修改弹层 ---------
let indexedit;
$('body').on('click','button:contains("编辑")',function(){
    //获取自定义属性
    //data不添加参数，获取所有属性值
    let data = $(this).data();
    // 注意接口文档的参数要求
    data.Id = data.id;
    indexedit = layer.open({
        type:1,
        title:'修改文章类别',
        area:['500px','250px'],
        content:$('#tpl-edit').html(),
        success:function(){
            let form = layui.form;
            //数据回填
            form.val('formedit',data);
        }
    });
});

//------------ ajax 请求完成修改功能 -----------
$('body').on('submit','.form-edit',function(e){
    e.preventDefault();
    let data = $(this).serialize();
    $.ajax({
        type:'post',
        url:'/my/article/updatecate',
        data,
        success:function(res){
            console.log(123)
            layer.msg(res.message);
            if(res.status === 0){
                renderSort();
                //添加成功关闭弹层
                layer.close(indexedit);
            }
        }
    });
});