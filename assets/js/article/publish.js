//初始化 表单模块
let form = layui.form;
//获取分类，并渲染到下拉栏的位置
$.ajax({
    url:'/my/article/cates',
    success:function(res){
        let html = template('tpl-category',res);
        $('select[name=cate_id]').html(html);
        //更新渲染
        form.render('select');
    }
});
//把内容区更换为富文本编辑器
initEditor();

//------------------ 使用剪裁插件 ---------------
//初始化剪裁框
let $image = $('#image');
let options = {
    //宽高比例
    aspectRatio: 400 / 280,
    //预览区容器的类名
    preview: '.img-preview'
}

// 初始化裁剪区域
$image.cropper(options)

//-------------- 图片选择 ---------------
$('button:contains("选择封面")').on('click',function(){
    $('#file').trigger('click');
});
$('#file').on('change',function(e){
    let fileObj = e.target.files[0];
    let url = URL.createObjectURL(fileObj);
    // 销毁旧的裁剪区域 重新设置图片路径 重新初始化裁剪区域    
    $image.cropper('destroy').attr('src', url).cropper(options) ; 
});

//------------- 完成添加功能 ------------
$('form').on('submit',function(e){
    e.preventDefault();
    //收集表单的数据(要收集文件)
    let fd = new FormData(this);
    //富文本编辑器替换formdata里面content的内容
    fd.set('content',tinyMCE.activeEditor.getContent());
    //剪裁图片
    let canvas = $image.cropper('getCroppedCanvas',{
        width:400,
        height:280
    });
    //把图片转成blob形式
    canvas.toBlob(function(blob) {       
        //函数的形参就是转换后的结果
        // 将 Canvas 画布上的内容，转化为文件对象
        //把文件追加到fd中
        fd.append('cover_img',blob);
        // 遍历fd对象，查看是否包含接口所要求的参数
        fd.forEach((value,key)=>{
        console.log(value,key);
        });
   //ajax 提交数据，完成添加
     $.ajax({
       type:'post',
       url:'/my/article/add',
       data:fd,
       //提交formData数据，必须有下面两个属性
       processData:false,
       contentType:false,
       success:function(res){
           layer.msg(res.message);
           if(res.status === 0){
            location.href='/article/article.html';
           }
       }
        });
    });
});