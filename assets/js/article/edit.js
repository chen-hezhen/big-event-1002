//初始化layui的form内置模块
let form = layui.form;
//获取地址栏的id参数值，这个id就是文章的id
//new URLSearchParams()将地址栏的参数解析为对象，有一系列的方法,只能用来处理地址参数
let id = new URLSearchParams(location.search).get('id');
//获取真实的分类，并渲染到下拉栏的位置,要下拉栏渲染完毕之后才能，完成数据回填，但是异步请求不能控制完成的先后顺序,此处只能先嵌套
$.ajax({
    url:'/my/article/cates',
    success:function(res){
        let html = template('tpl-category',res);
        $('select[name=cate_id]').html(html);
        //更新渲染
        form.render('select');
        //分类渲染完毕，可以获取文章详情
        //请求当前这篇文章的详情，完成数据回填
    $.ajax({
    url:'/my/article/' + id,
    success:function(res){
        if(res.status === 0){
        //使用layui，form模块中 的val方法，快速为表单赋值
        form.val('xxx',res.data);
        console.log(123456);
        // 先完成数据回填把内容区更换为富文本编辑器
        initEditor();
        // 更换图片
        $image.cropper('destroy').attr('src', 'http://ajax.frontend.itheima.net'+res.data.cover_img).cropper(options) ;  

        }
    }
});
    }
});

// ----------- 裁剪插件使用 ----------
//初始化剪裁框
let $image = $('#image');
let options = {
    //宽高比例
    aspectRatio: 400 / 280,
    //预览区容器的类名
    preview: '.img-preview',
    autoCropArea:1

}
// 初始化裁剪区域
$image.cropper(options);

//----------- 选择图片 ------------
$('button:contains("选择封面")').on('click',function(){
    $('#file').trigger('click');
});
$('#file').on('change',function(e){
    let fileObj = e.target.files[0];
    //创建图片的临时url
    let url = URL.createObjectURL(fileObj);
    // 销毁旧的裁剪区域 重新设置图片路径 重新初始化裁剪区域  
    $image.cropper('destroy').attr('src', url).cropper(options) ;   
});
//----------------- 提交表单信息 ----------------
$('form').on('submit',function(e){
    e.preventDefault();
    //收集表单的数据(要收集文件)
    let fd = new FormData(this);
    //富文本编辑器替换formdata里面content的内容
    fd.set('content',tinyMCE.activeEditor.getContent());
    //剪裁图片
    let canvas = $image.cropper('getCroppedCanvas',{
        width:400,
        height:280
    });
    canvas.toBlob(function(blob) {       
        //函数的形参就是转换后的结果
        // 将 Canvas 画布上的内容，转化为文件对象
        //把文件追加到fd中
        fd.append('cover_img',blob);
        //根据接口要求，修改文章的时候，formdata中必须有文章的id
        fd.append('Id',id);

   //-------------- 发送请求 完成修改 -----------
     $.ajax({
       type:'post',
       url:'/my/article/edit',
       data:fd,
       //提交formdata数据，必须有下面两个选项
       processData:false,
       contentType:false,
       success:function(res){
           layer.msg(res.message);
           if(res.status === 0){
            location.href='/article/article.html'
           }
       }
        });
    });
 
});