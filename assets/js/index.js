// --------------- 获取用户信息，渲染页面 -----------------
function render(){
    $.ajax({
        url:'/my/userinfo',
        success:function(res){
            if(res.status === 0){
                //渲染用户的用户名（优先使用nickname，新注册的用户还没有添加nickname，使用username）
                let name = res.data.nickname || res.data.username;
                $('.username').html(name);
                //渲染用户的头像
                if(res.data.user_pic){
                    console.log(123);
                    //有图片
                    $('.layui-nav-img').attr('src',res.data.user_pic).show();
                    $('.avater').hide();
                }else{
                    //没有添加头像图片，截取用户名中的第一个字符作为头像中的字
                    let first = name.substr(0,1).toUpperCase();
                    $('.avatar').text(first).show().css('display','inline-block');
                    $('.layui-nav-img').hide();
                }
                    
                
            }
        }
    });
}
render();

//退出功能，点击退出询问，删除token,跳转到login.html
$('#logout').on('click',function(){
    //弹出层可以不加载，直接使用
    layer.confirm('确定退出吗?', {icon: 3, title:'提示'}, function(index){
        //do something
        //移除token
        localStorage.removeItem('token');
        //跳转到login.html
        location.href = '/login.html';
        layer.close(index);//关闭弹层
      });
});