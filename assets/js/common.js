//统一配置 ajax请求
$.ajaxPrefilter(function(option){
    //统一配置URL根目录
    option.url='http://ajax.frontend.itheima.net'+option.url;
    //判断url里面有没有/my 没有的话就不需要访问权限也就是不需要token
    if(option.url.includes('/my/')){
        //统一配置请求头
        option.headers = {
            Authorization:localStorage.getItem('token'),
        }
    }
    //ajax请求完成
    option.complete = function(xhr){
       if(xhr.responseJSON.status === 1 && xhr.responseJSON.message === '身份认证失败！') {
        //如果为真，那么用户使用了过期的或者假的token
        //删除假的token
        localStorage.removeItem('token');
        //跳转到登录界面
        location.href = '/login.html';
        }
    }
});