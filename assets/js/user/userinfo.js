//加载layui的表单模块
let form = layui.form;
//------------------ 数据回填 ，给表单默认值 ----------------
function renderForm(){
    $.ajax({
        url:'/my/userinfo',
        success:function(res){
            /*
            使用内置方法,form.val('formTest',res.data)中的formTest 和 class="layui-form"所在的元素属性 lay-filter="" 对应的值，两个值可以任意取，但是一定要相同
            */
            form.val('formTest',res.data);
        }
    });
}
renderForm();

//--------------- 完成提交个人基本资料的添加和修改操作 -------------
$('form').on('submit',function(e){
    e.preventDefault();
    let data = $(this).serialize();
    $.ajax({
        type:'post',
        url:'/my/userinfo',
        data,
        success:function(res){
            layer.msg(res.message);
            if(res.status === 0){
                //调用父级页面的渲染函数，重新渲染头像
                window.parent.render();
            }
        }
    });
});

//--------------- 点击重置，清空页面资料信息 ------------- 
$('button[type = reset]').on('click',function(e){
    e.preventDefault();
    renderForm();
});