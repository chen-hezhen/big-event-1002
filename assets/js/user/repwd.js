//------------ 自定义验证规则 ----------
//加载layui内置form模块
let form = layui.form;

form.verify({
        //验证输入密码长度
        len:[/\S{6,12}/,'密码的长度必须是6-12位'],
        
        //验证新密码不能和就密码相同
        diff:function(val){
            //形参表示的是输入的新密码
            //获取原密码
            let oldpwd = $('.oldpwd').val();
            if(oldpwd === val){
                return '新密码和原密码不能相同';
            }
        },

        //验证新密码和确认密码相同
        same:function(val){
            let newpwd = $('.newpwd').val();
            if(newpwd !== val){
                return '两次密码输入不同，请从新输入';
            }
        }
});

//------------  完成密码更改 ---------
$('form').on('submit',function(e){
    //阻止表单默认跳转事件
    e.preventDefault();
    //收集表单信息
    let data = $(this).serialize();
    $.ajax({
        type:'post',
        url:'/my/updatepwd',
        data,
        success:function(res){
            layer.msg(res.message);
            if(res.status === 0){
                //清空输入框的值,dom方法
                $('form')[0].reset();
            }
        }
    });
});