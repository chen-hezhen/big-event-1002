
//--------- 实现基本的剪裁效果 ----------
//创建剪裁区
//找到剪裁区的图片
var $image = $('#image');
var option = {
    //纵横比（宽高比）
    aspectRatio:1,//正方形
    //指定预览区域
    preview:'.img-preview',//指定预览区的类名（选择器）
};
//调用cropper方法，创建剪裁区
$image.cropper(option);

//--------- 点击选择图片 选择图片 --------
//点击按钮触发选择文件是事件
$('button:contains("选择图片")').on('click',function(){
    $('#file').trigger('click');
});

//更换剪裁区的图片
$('#file').on('change',function(){
    //找到文件对象
    let fileObj = this.files[0];
    //为文件对象生成一个新的url
    let url = URL.createObjectURL(fileObj);
    //先销毁剪裁区 - 更换剪裁区- 重建剪裁区 -更换剪裁区图片
    $image.cropper('destroy').attr('src',url).cropper(option);
});

// ------------ 点击确定按钮 完成更换 -----------------
$('button:contains("确定")').on('click',function(){
    //完成裁剪，得到canvas图片
    let canvas = $image.cropper('getCroppedCanvas',{
        width:100,
        height:100
    });
    //图片转成字符串形式
    let str = canvas.toDataURL('image/png');
    //ajax 提交字符串
    $.ajax({
        type:'post',
        url:'/my/update/avatar',
        data:{avatar:str},
        success:function(res){
            layer.msg(res.message);
            if(res.status === 0){
                window.parent.render();
            }
        }
    });
});

// 表单的默认值
function Avatar(){
    //发送请求 获取用户信息
    $.ajax({
        url:'/my/userinfo',
        success:function(res){
            if(res.data.user_pic){
              $image.cropper('destroy').attr('src',res.data.user_pic).cropper(option);   
            }
        }
    });
}
Avatar();
